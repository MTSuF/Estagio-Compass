# Estágio-Compass

## Bem-vindo ao Repositório da Sprint 2!

Este repositório foi criado para documentar minha jornada de aprendizado e compartilhar os conhecimentos adquiridos durante a segunda sprint do projeto.

## :point_right: Índice

### Sprint 2

- [Dia 01](https://gitlab.com/MTSuF/Estagio-Compass/-/blob/pb_sprint2/Sprint%202/dia01-20-05.md?ref_type=heads) - Conceitos HTTP, API REST, JSON, User Stories & Issues 
- [Dia 02](https://gitlab.com/MTSuF/Estagio-Compass/-/blob/pb_sprint2/Sprint%202/dia02-21-05.md?ref_type=heads) - Testes Estáticos (Swagger)
- [Dia 03](https://gitlab.com/MTSuF/Estagio-Compass/-/blob/pb_sprint2/Sprint%202/dia03-22-05.md?ref_type=heads) - Tipos de erros, validações e boas práticas em testes de API
- [Dia 04](https://gitlab.com/MTSuF/Estagio-Compass/-/blob/pb_sprint2/Sprint%202/dia04-23-05.md?ref_type=heads) - Planejamento de testes
- [Dia 05](https://gitlab.com/MTSuF/Estagio-Compass/-/blob/pb_sprint2/Sprint%202/dia05-24-25.md?ref_type=heads) - Cobertura de testes de APIs & Testes candidatos à automação
- [Dia 06](https://gitlab.com/MTSuF/Estagio-Compass/-/blob/pb_sprint2/Sprint%202/dia05-24-25.md?ref_type=heads) - Introdução às atividades de análise de teste & Priorização em diferentes aplicações
- [Dia 07](https://gitlab.com/MTSuF/Estagio-Compass/-/blob/pb_sprint2/Sprint%202/dia06-27-05.md?ref_type=heads) - Análise HTTP e API REST
- [Dia 08](https://gitlab.com/MTSuF/Estagio-Compass/-/blob/pb_sprint2/Sprint%202/dia08-29-05.md?ref_type=heads) - Introdução ao Postman
- [Challenge](https://gitlab.com/MTSuF/Estagio-Compass/-/blob/pb_sprint2/Sprint%202/Challange.md?ref_type=heads) - Challenge
- [Resultado de testes](https://gitlab.com/MTSuF/Estagio-Compass/-/blob/pb_sprint2/Sprint%202/Resultado_de_testes.md?ref_type=heads) - Resultado de testes

## :handshake: Contribuições 
- Gabriel Just
- Ricardo Enio Neckel
- Jorge Soares de Carvalho
- Letícia Ribeiro de Souza Barbosa dos Santos
- Enzo Rossi

## Autor
- Mathias Uecker Fischer
<img src="https://github.com/MTSuF/Estagio-Compass/assets/129664506/fb2acdb7-b7ac-4b45-929e-b77ea928f994" alt="Mathias Uecker Fischer" width="250" height="301">

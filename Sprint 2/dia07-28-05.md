# SPRINT 02 DIA 07

## Análise HTTP e API REST

### Pontos Chave
- **HTTP e REST:** Fundamentos do protocolo HTTP e como ele é utilizado em APIs REST.
- **Métodos HTTP:** Compreensão dos métodos HTTP (GET, POST, PUT, DELETE) e seus usos em operações CRUD.
- **Status Codes:** Importância dos códigos de status HTTP para indicar o resultado das requisições.
- **Headers HTTP:** Uso de headers para fornecer metadados e autenticação em requisições.

### Principais Aprendizados
- **Design de APIs REST:** Boas práticas para desenhar APIs RESTful eficientes e escaláveis.
- **Segurança em APIs:** Implementação de autenticação e autorização usando tokens JWT e HTTPS.
- **Documentação:** Utilização de ferramentas como Swagger para documentar e testar APIs.

### Reflexões
- Como podemos melhorar a segurança das nossas APIs REST?
- Quais métodos podemos implementar para garantir a escalabilidade das nossas APIs?

### Próximos Passos
- **Auditoria de Segurança:** Realizar auditorias de segurança periódicas para identificar e corrigir vulnerabilidades.
- **Automação de Testes:** Desenvolver scripts para testes automatizados das APIs para assegurar consistência e qualidade.
- **Monitoramento de API:** Implementar ferramentas de monitoramento para acompanhar o desempenho e a disponibilidade das APIs.

## Mapa mental

![Mapa Mental](<../images/API Serverest - Sprint Atual.png>)
# Resultados de testes

## Cadastro de Usuários

1. **Positivos:**

    - Cadastro de usuário com dados válidos.
    
    ![RT_01](../images/RT_01.png)

2. **Negavtivos:**

    - Cadastro com email já existente.

    ![RT_02](../images/RT_02.png)

    - Cadastro com provedores restritos (gmail, hotmail)

    ![RT_03](../images/RT_03.png)

   ## [Bug report](https://gitlab.com/MTSuF/Estagio-Compass/-/issues/1)

    - Cadastro com campos obrigatórios faltando.

    ![RT_04](../images/RT_04.png)

    - Cadastro com senha fora dos limites especificados.

    ![RT_05](../images/RT_05.png)

    ![RT_06](../images/RT_06.png)

   ## [Bug report](https://gitlab.com/MTSuF/Estagio-Compass/-/issues/2)

## Autenticação

1. **Positivos:**

    - Login com credenciais válidas.

    ![RT_07](../images/RT_07.png)

2. **Negavtivos:**

    - Login com senha incorreta

    ![RT_08](../images/RT_08.PNG)

    - Login com email inexistente.

    ![RT_09](../images/RT_09.PNG)

## Manipulação de Usuários

1. **Positivos:**

    - Atualização de dados de um usuário existente.

    ![RT_10](../images/RT_10.PNG)

    - Exclusão de um usuário.

    ![RT_11](../images/RT_11.PNG)

2. **Negavtivos:**

    - Atualização de um usuário inexistente.

    ![RT_12](../images/RT_12.PNG)

    - Exclusão de um usuário inexistente.

    ![RT_13](../images/RT_13.PNG)

## Listagem de Usuários

1. **Positivos:**

    - Obter a lista completa de usuários.

    ![RT_14](../images/RT_14.PNG)

    - Obter detalhes de um usuário específico.

    ![RT_15](../images/RT_15.PNG)

2. **Negavtivos:**

    - Obter detalhes de um usuário inexistente.

    ![RT_16](../images/RT_16.PNG)
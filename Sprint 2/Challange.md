# Challenge

## Planejamento de Testes para API ServeRest


### Objetivo

Garantir a qualidade e robustez das funcionalidades da API ServeRest através de testes sistemáticos e abrangentes, cobrindo tanto cenários positivos quanto negativos.

### Estratégia de Testes

#### Abordagem
1. Exploratória: Entender o comportamento da API, identificando pontos críticos e funcionalidades principais.
2. Funcional: Validar que a API funciona conforme especificado.
3. Negativa: Garantir que a API responde corretamente a entradas inválidas.

### Acceptance Criteria

- Os vendedores (usuários) deverão possuir os campos NOME, E-MAIL, PASSWORD e ADMINISTRADOR;
- Não deverá ser possível fazer ações e chamadas para usuários inexistentes;
- Não deve ser possível criar um usuário com e-mail já utilizado;
- Caso não seja encontrado usuário com o ID informado no PUT, um novo usuário deverá ser criado;
- Não deve ser possível cadastrar usuário com e-mail já utilizado utilizando PUT;
- Os testes executados deverão conter evidências;
- Não deverá ser possível cadastrar usuários com e-mails de provedor gmail e hotmail;
- Os e-mails devem seguir um padrão válido de e-mail para o cadastro;
- As senhas devem possuír no mínimo 5 caracteres e no máximo 10 caracteres;
- A cobertura de testes deve se basear no Swagger e ir além, cobrindo cenários alternativos.

### Cenários e seus Casos de Teste

#### Cadastro de Usuários

1. **Positivos:**
    - Cadastro de usuário com dados válidos.
2. **Negavtivos:**
    - Cadastro com email já existente.
    - Cadastro com provedores restritos (gmail, hotmail)
    - Cadastro com campos obrigatórios faltando.
    - Cadastro com senha fora dos limites especificados.

#### Autenticação

1. **Positivos:**
    - Login com credenciais válidas.
2. **Negavtivos:**
    - Login com senha incorreta.
    - Login com email inexistente.

#### Manipulação de Usuários

1. **Positivos:**
    - Atualização de dados de um usuário existente.
    - Exclusão de um usuário.
2. **Negavtivos:**
    - Atualização de um usuário inexistente.
    - Exclusão de um usuário inexistente.

#### Listagem de Usuários

1. **Positivos:**
    - Obter a lista completa de usuários.
    - Obter detalhes de um usuário específico.
2. **Negavtivos:**
    - Obter detalhes de um usuário inexistente.

### Prioridades

#### Alta Prioridade

    - Cadastro de usuários (positivos e negativos)
    - Autenticação de usuários
    - Listagem de usuários
    - Testes de segurança
#### Média Prioridade

    - Atualização de usuários
    - Exclusão de usuários
    - Testes de desempenho
#### Baixa Prioridade

    - Testes de usabilidade

### Testes Candidatos para Automação

#### Funcionalidades Essenciais

- Cadastro de usuários: Automatizar o cadastro de usuários permite validar consistentemente a criação de novos usuários, garantindo que todas as validações de campos e regras de negócios sejam aplicadas corretamente sem falhas humanas.
    
- Autenticação: A autenticação é crítica para a segurança. Automatizar este processo assegura que as verificações de login, como credenciais corretas e erradas, sejam executadas rigorosamente, identificando brechas de segurança de forma contínua.

- Listagem de usuários: A listagem de usuários é uma funcionalidade recorrente. Automatizar este teste ajuda a garantir que os dados retornados sejam consistentes e precisos, facilitando a detecção de problemas de performance e consistência de dados

#### Cenários Repetitivos

- Verificação de mensagens de erro para entradas inválidas: Mensagens de erro são essenciais para a experiência do usuário e a depuração. Automatizar testes de entradas inválidas assegura que todas as mensagens de erro sejam coerentes e informativas, mantendo a qualidade da interface e do backend.

- Testes de regressão: Testes de regressão garantem que novas alterações no código não quebrem funcionalidades existentes. Automatizar esses testes economiza tempo e esforço, proporcionando uma forma eficiente de verificar a integridade do sistema após cada atualização ou modificação.
# SPRINT 02 DIA 03

## Tópicos

### 1. Validações de APIs

#### 1.1. Validação de Requisições
- **Validação de Campos Obrigatórios:**
  - Verificar se todos os campos obrigatórios estão presentes na requisição.
- **Validação de Tipos de Dados:**
  - Garantir que os dados enviados possuem os tipos corretos (string, número, boolean, etc.).
- **Validação de Formato:**
  - Verificar o formato dos dados (por exemplo, e-mails, URLs, datas).
- **Validação de Limites e Restrições:**
  - Validar limites numéricos, tamanhos de strings, e outras restrições específicas do campo.

#### 1.2. Validação de Respostas
- **Status Codes:**
  - Garantir que a API retorna os códigos de status HTTP corretos (200, 201, 400, 401, 404, 500, etc.).
- **Estrutura do Corpo da Resposta:**
  - Verificar se a estrutura do JSON retornado está conforme o esperado.
- **Dados Retornados:**
  - Validar se os dados retornados são corretos e consistentes com a requisição feita.
- **Cabeçalhos HTTP:**
  - Verificar a presença de cabeçalhos HTTP importantes (Content-Type, Authorization, etc.).

### 2. Boas Práticas para Testes de APIs

#### 2.1. Estrutura e Organização dos Testes
- **Modularização dos Testes:**
  - Dividir os testes em módulos claros e organizados por funcionalidades.
- **Reutilização de Código:**
  - Criar funções utilitárias para operações comuns (autenticação, criação de dados, etc.).

#### 2.2. Automatização dos Testes
- **Ferramentas de Teste:**
  - Utilizar ferramentas como Postman, Newman, JUnit, RestAssured, ou outras específicas para testes de API.
- **Integração Contínua:**
  - Integrar os testes de API no pipeline de CI/CD para garantir a execução automática e regular.
- **Testes Automatizados:**
  - Escrever testes automatizados para cobrir os principais endpoints e cenários da API.

#### 2.3. Cobertura de Testes
- **Testes Funcionais:**
  - Cobrir todas as funcionalidades da API com testes que validem o comportamento esperado.
- **Testes de Carga:**
  - Realizar testes de carga para verificar o desempenho da API sob alta demanda.
- **Testes de Segurança:**
  - Verificar a segurança da API com testes que simulam ataques comuns (SQL Injection, XSS, etc.).
- **Testes de Compatibilidade:**
  - Garantir que a API funciona corretamente em diferentes ambientes e com diferentes versões de dependências.

#### 2.4. Boas Práticas de Documentação
- **Documentação Clara e Completa:**
  - Manter a documentação da API atualizada e completa, incluindo exemplos de requisições e respostas.
- **Utilização de Swagger:**
  - Utilizar Swagger ou ferramentas similares para documentar a API de forma interativa e acessível.

#### 2.5. Monitoramento e Logs
- **Monitoramento em Tempo Real:**
  - Implementar monitoramento para detectar falhas e problemas de desempenho em tempo real.
- **Logs Detalhados:**
  - Manter logs detalhados das requisições e respostas para facilitar a identificação e resolução de problemas.

## Como fazer Testes Funcionais em API Rest

### 1. Introdução aos Testes Funcionais em API Rest
- **Objetivo:** Entender a importância dos testes funcionais para APIs Rest.
- **Atividades:**
  - Introduzir o conceito de testes funcionais.
  - Discutir como identificar o que precisa ser testado em uma API.

### 2. Documentação com Swagger
- **Objetivo:** Utilizar Swagger para documentar e entender a API.
- **Atividades:**
  - Revisar a documentação gerada pelo Swagger.
  - Explorar como usar o Swagger para testar e validar endpoints.

### 3. Técnicas de Teste Funcional
- **Objetivo:** Aprender técnicas de teste funcional específicas para APIs.
- **Atividades:**
  - Demonstrar o uso de testes baseados em técnicas e testes formais.
  - Aplicar essas técnicas para verificar a funcionalidade de uma API.

### 4. Criando um Plano de Teste
- **Objetivo:** Desenvolver um plano de teste para uma API.
- **Atividades:**
  - Identificar os cenários de teste principais.
  - Estabelecer critérios de sucesso e falha para os testes.

### 5. Utilizando Ferramentas de Teste
- **Objetivo:** Familiarizar-se com ferramentas comuns para testar APIs.
- **Atividades:**
  - Demonstrar o uso de ferramentas como Postman e Insomnia.
  - Comparar as funcionalidades dessas ferramentas na realização de testes.

### 6. Testando Regras de Negócio
- **Objetivo:** Validar as regras de negócio implementadas na API.
- **Atividades:**
  - Criar testes específicos para verificar se as regras de negócio estão sendo corretamente aplicadas.
  - Analisar exemplos de regras de negócio e como testá-las.

### 7. Testando Autenticação e Autorização
- **Objetivo:** Garantir que a autenticação e a autorização estão corretamente implementadas.
- **Atividades:**
  - Criar testes para verificar se apenas usuários autorizados podem acessar determinados endpoints.
  - Testar diferentes cenários de autenticação e autorização.

### 8. Manipulando Dados de Teste
- **Objetivo:** Aprender a preparar e manipular dados de teste para APIs.
- **Atividades:**
  - Configurar dados de teste necessários para validar as operações da API.
  - Demonstrar como usar dados de teste para simular diferentes condições.

### 9. Analisando Respostas da API
- **Objetivo:** Validar as respostas retornadas pela API.
- **Atividades:**
  - Verificar se os dados retornados pela API estão corretos e completos.
  - Testar diferentes cenários de resposta, incluindo erros.

### 10. Automatizando Testes de API
- **Objetivo:** Implementar a automação de testes para APIs.
- **Atividades:**
  - Introduzir ferramentas e frameworks para automação de testes de API.
  - Demonstrar como configurar e executar testes automatizados.

### 11. Testes de Carga e Desempenho
- **Objetivo:** Avaliar o desempenho da API sob diferentes condições de carga.
- **Atividades:**
  - Configurar e executar testes de carga para simular vários usuários acessando a API.
  - Analisar os resultados para identificar possíveis gargalos e otimizações.

### 12. Integração Contínua e Testes de API
- **Objetivo:** Integrar testes de API no pipeline de integração contínua.
- **Atividades:**
  - Configurar testes de API para serem executados automaticamente durante o processo de CI/CD.
  - Monitorar e analisar os resultados dos testes integrados.

### 13. Reportando Resultados de Testes
- **Objetivo:** Aprender a documentar e reportar os resultados dos testes de API.
- **Atividades:**
  - Criar relatórios detalhados sobre os testes realizados e os resultados obtidos.
  - Utilizar ferramentas para gerar relatórios automatizados.

### 14. Testes de Segurança em APIs
- **Objetivo:** Garantir a segurança das APIs através de testes específicos.
- **Atividades:**
  - Identificar vulnerabilidades comuns em APIs.
  - Demonstrar técnicas para testar a segurança e proteger a API.

### 15. Melhores Práticas em Testes de API
- **Objetivo:** Adotar as melhores práticas para realizar testes eficazes em APIs.
- **Atividades:**
  - Discutir boas práticas na configuração e execução de testes.
  - Analisar casos de estudo e exemplos reais de testes bem-sucedidos.

### 16. Resolvendo Problemas Comuns em Testes de API
- **Objetivo:** Abordar problemas comuns e suas soluções durante os testes de API.
- **Atividades:**
  - Identificar desafios frequentes encontrados em testes de API.
  - Apresentar soluções e práticas recomendadas para resolver esses problemas.

## Tipos de erros (Back-End)

### 1. Erros de Sintaxe
- **Descrição:** Ocorrências de código malformado ou incorretamente estruturado que impedem a execução do programa.
- **Exemplo:** Falta de ponto e vírgula, uso incorreto de chaves, parênteses não fechados.

### 2. Erros de Tempo de Execução (Runtime Errors)
- **Descrição:** Erros que ocorrem durante a execução do programa, após a compilação bem-sucedida.
- **Exemplo:** Divisão por zero, acesso a um índice inexistente em um array.

### 3. Erros de Compilação
- **Descrição:** Erros que impedem a compilação do código, geralmente devido a problemas de sintaxe ou tipos de dados incorretos.
- **Exemplo:** Tipos de dados incompatíveis, funções chamadas incorretamente.

### 4. Erros de Lógica
- **Descrição:** Erros no raciocínio do código, onde o programa não se comporta conforme o esperado.
- **Exemplo:** Laços infinitos, condições erradas em instruções if.

### 5. Erros de Dependência
- **Descrição:** Ocorrências de falha devido à falta ou incompatibilidade de bibliotecas ou pacotes necessários.
- **Exemplo:** Tentativa de usar uma biblioteca que não está instalada ou que possui uma versão diferente da esperada.

### 6. Erros de Configuração
- **Descrição:** Problemas relacionados à configuração do ambiente de desenvolvimento ou produção.
- **Exemplo:** Variáveis de ambiente incorretas, configuração de banco de dados errada.

### 7. Erros de Conexão
- **Descrição:** Falhas na comunicação com outros serviços, como bancos de dados, APIs ou servidores externos.
- **Exemplo:** Timeout ao tentar se conectar a um banco de dados, falha de autenticação ao chamar uma API externa.

### 8. Erros de Autenticação/Autorização
- **Descrição:** Problemas que ocorrem quando usuários não autenticados ou não autorizados tentam acessar recursos restritos.
- **Exemplo:** Tentativa de acesso sem token JWT válido, permissões insuficientes para acessar um endpoint específico.

### 9. Erros de Banco de Dados
- **Descrição:** Falhas relacionadas às operações de banco de dados, como consultas malformadas ou transações falhas.
- **Exemplo:** Consultas SQL incorretas, deadlocks em transações.

### 10. Erros de Manipulação de Arquivos
- **Descrição:** Problemas que ocorrem ao tentar ler, escrever ou manipular arquivos no sistema de arquivos.
- **Exemplo:** Tentativa de abrir um arquivo inexistente, permissões insuficientes para escrever em um diretório.

### 11. Erros de Performace
- **Descrição:** Problemas que afetam o desempenho do sistema, tornando-o lento ou não responsivo.
- **Exemplo:** Algoritmos ineficientes, uso excessivo de recursos do sistema.

### 12. Erros de Segurança
- **Descrição:** Vulnerabilidades ou falhas que podem ser exploradas para comprometer a segurança do sistema.
- **Exemplo:** Injeção SQL, cross-site scripting (XSS).

### 13. Erros de Concurrency
- **Descrição:** Problemas que surgem ao lidar com execução paralela ou concorrente de processos.
- **Exemplo:** Condições de corrida, deadlocks.

### 14. Erros de Timeout
- **Descrição:** Falhas que ocorrem quando uma operação demora mais do que o tempo limite permitido.
- **Exemplo:** Tempo de resposta do banco de dados excede o limite configurado.

## Reflexão: Atuação Preventiva sobre Diferentes Tipos de Erros

### Introdução
Podemos classificar os erros em diferentes tipos segundo diversos critérios. Já percebemos que o nosso trabalho como embaixadores da qualidade no projeto não é apenas testar e encontrar erros, mas também prevenir de maneira a poupar tempo e esforço da equipe.

## Tipos de Erros e Estratégias Preventivas

### 1. Erros de Sintaxe
- **Prevenção:**
  - **Revisões de Código:** Implementar revisões de código rigorosas, onde os pares revisam o trabalho uns dos outros.
  - **Ferramentas de Linters:** Utilizar linters e ferramentas de análise estática de código para detectar erros de sintaxe antes da execução.

### 2. Erros de Tempo de Execução (Runtime Errors)
- **Prevenção:**
  - **Testes Unitários:** Escrever testes unitários abrangentes para verificar o comportamento do código em tempo de execução.
  - **Verificações de Tipo:** Usar linguagens de programação com verificação de tipo forte ou adicionar bibliotecas de verificação de tipo.

### 3. Erros de Compilação
- **Prevenção:**
  - **Ambiente de Desenvolvimento Integrado (IDE):** Utilizar IDEs que fornecem feedback imediato sobre erros de compilação.
  - **Automação de Build:** Configurar pipelines de build automatizados que compilam o código a cada commit.

### 4. Erros de Lógica
- **Prevenção:**
  - **Revisões de Código:** Realizar revisões de código focadas em lógica e fluxo do programa.
  - **Design e Planejamento:** Investir mais tempo no design e planejamento das funcionalidades antes da implementação.

### 5. Erros de Dependência
- **Prevenção:**
  - **Gerenciamento de Dependências:** Utilizar ferramentas de gerenciamento de dependências que garantam a instalação correta das versões necessárias.
  - **Atualizações Regulares:** Manter as dependências atualizadas e realizar verificações de compatibilidade regularmente.

### 6. Erros de Configuração
- **Prevenção:**
  - **Documentação:** Documentar todas as configurações necessárias para diferentes ambientes (desenvolvimento, teste, produção).
  - **Automatização:** Usar ferramentas de gerenciamento de configuração para aplicar configurações de forma consistente em todos os ambientes.

### 7. Erros de Conexão
- **Prevenção:**
  - **Mocking e Stubbing:** Utilizar mocks e stubs durante o desenvolvimento e testes para simular conexões externas.
  - **Monitoramento:** Implementar monitoramento contínuo das conexões para detectar problemas rapidamente.

### 8. Erros de Autenticação/Autorização
- **Prevenção:**
  - **Revisões de Segurança:** Realizar revisões de segurança regulares no código de autenticação e autorização.
  - **Testes de Penetração:** Executar testes de penetração para identificar e corrigir vulnerabilidades.

### 9. Erros de Banco de Dados
- **Prevenção:**
  - **Otimização de Consultas:** Revisar e otimizar consultas SQL para garantir eficiência.
  - **Testes de Integração:** Escrever testes de integração que interajam diretamente com o banco de dados.

### 10. Erros de Manipulação de Arquivos
- **Prevenção:**
  - **Validação de Arquivos:** Validar todos os arquivos antes de processá-los.
  - **Permissões Adequadas:** Garantir que o sistema tenha permissões adequadas para acessar e manipular arquivos.

### 11. Erros de Performance
- **Prevenção:**
  - **Profiling:** Utilizar ferramentas de profiling para identificar gargalos de desempenho.
  - **Testes de Performance:** Realizar testes de carga e stress regularmente.

### 12. Erros de Segurança
- **Prevenção:**
  - **Auditorias de Segurança:** Realizar auditorias de segurança periódicas.
  - **Treinamento de Segurança:** Treinar a equipe sobre práticas de segurança e conscientização de ameaças.

### 13. Erros de Concurrency
- **Prevenção:**
  - **Design Thread-Safe:** Projetar sistemas que sejam seguros para execução concorrente.
  - **Testes de Concurrency:** Escrever testes específicos para cenários de execução concorrente.

### 14. Erros de Timeout
- **Prevenção:**
  - **Timeouts Adequados:** Configurar tempos limites adequados para operações críticas.
  - **Retentativas:** Implementar lógica de retentativa para operações que falham devido a timeouts.

## Reflexões Finais
Prevenir erros não é apenas uma tarefa técnica, mas envolve uma mudança de mentalidade na equipe. A colaboração, a comunicação clara e o investimento em ferramentas e práticas de qualidade são essenciais para minimizar erros e aumentar a eficiência do desenvolvimento.

## Próximos Passos
- **Discussão em Equipe:** Conversar com a equipe sobre as estratégias apresentadas e adaptar as práticas ao contexto do projeto.
- **Implementação Gradual:** Introduzir as práticas preventivas de forma gradual, priorizando as áreas mais críticas.
- **Avaliação Contínua:** Monitorar e avaliar continuamente a eficácia das práticas implementadas e fazer ajustes conforme necessário.

## Conclusão

## Pontos Chave

1. **Validações de APIs:** Aprendemos a importância de realizar validações robustas tanto nas requisições quanto nas respostas das APIs, garantindo a integridade e a consistência dos dados trocados.
2. **Boas Práticas para Testes de APIs:** Exploramos as melhores práticas para estruturar, automatizar e documentar testes de APIs, além de compreender a importância do monitoramento e dos logs.
3. **Testes Funcionais em API Rest:** Discutimos técnicas de teste funcional, desde a criação de planos de teste até a utilização de ferramentas específicas para testes de APIs.
4. **Tipos de Erros no Back-End:** Revisamos os diversos tipos de erros que podem ocorrer no back-end, desde erros de sintaxe até problemas de configuração e segurança.

## Principais Aprendizados

- **Importância das Validações:** As validações são cruciais para garantir que as APIs funcionem corretamente e que os dados manipulados sejam válidos e seguros.
- **Automatização e Integração Contínua:** Automatizar testes e integrá-los ao pipeline de CI/CD melhora a eficiência do desenvolvimento e a qualidade do software.
- **Compreensão de Erros:** Conhecer os tipos de erros comuns no back-end ajuda na identificação e resolução rápida de problemas, melhorando a robustez do sistema.

## Reflexões e Próximos Passos

A jornada de hoje destacou a importância de práticas bem estruturadas e a automação nos testes de APIs. À medida que avançamos, devemos focar em aprofundar nosso entendimento sobre ferramentas de teste e explorar mais casos reais de aplicação dessas práticas. Continuar aprimorando nossas habilidades de teste e validação será fundamental para garantir a qualidade e a segurança de nossas APIs.